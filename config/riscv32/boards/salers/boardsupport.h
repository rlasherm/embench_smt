/* Copyright (C) 2017 Embecosm Limited and University of Bristol

   Contributor Graham Markall <graham.markall@embecosm.com>

   This file is part of Embench and was formerly part of the Bristol/Embecosm
   Embedded Benchmark Suite.

   SPDX-License-Identifier: GPL-3.0-or-later */

#include <stdint.h>

#define CPU_MHZ 1


volatile void hsc_switch_in_place(uint32_t id, uint32_t caps);
volatile uint32_t get_hart_id();
volatile uint32_t get_hart_count();
void infloop();
void pseudo_atomic_ret_write(void* address);

volatile void wait_for_sync_loop();
void wait_for_sync();
volatile void sync();
volatile void write_u32(uint32_t val, void* add);