/* Copyright (C) 2017 Embecosm Limited and University of Bristol

   Contributor Graham Markall <graham.markall@embecosm.com>

   This file is part of Embench and was formerly part of the Bristol/Embecosm
   Embedded Benchmark Suite.

   SPDX-License-Identifier: GPL-3.0-or-later */

#include <support.h>
#include "boardsupport.h"

const uint32_t jump_minus4 = 0xffdff06f;
const uint32_t jump_minus20 = 0xfedff06f;

void wait_for_sync() {
    wait_for_sync_loop();

    //reset waiter
    //point to jump instruction
    uint32_t* add = &((uint32_t*)wait_for_sync_loop)[5];
    // *add = jump_minus4;
    *add = jump_minus20;
    // write_u32(jump_minus20, (void*)add);
}

volatile void sync() {
    //point to jump instruction
    uint32_t* add = &((uint32_t*)wait_for_sync_loop)[5];
    pseudo_atomic_ret_write((void*)add);
}

void
initialise_board ()
{
  __asm__ volatile ("li a0, 0" : : : "memory");
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
start_trigger ()
{
  __asm__ volatile ("li a0, 0" : : : "memory");
  __asm__ volatile ("csrrw x31, cycle, x0" : : : "memory");
  __asm__ volatile ("li x29, 0x101010" : : : "memory");
  __asm__ volatile ("li x30, 0" : : : "memory");
  __asm__ volatile ("fence.i" : : : "memory");
  __asm__ volatile ("li x29, 0" : : : "memory");
  __asm__ volatile ("li x30, 0" : : : "memory");
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
stop_trigger ()
{
  __asm__ volatile ("li a0, 0" : : : "memory");
  __asm__ volatile ("csrrw x31, cycle, x0" : : : "memory");
  __asm__ volatile ("li x29, 0x101010" : : : "memory");
  __asm__ volatile ("li x30, 1" : : : "memory");
  __asm__ volatile ("fence.i" : : : "memory");
  __asm__ volatile ("li x29, 0" : : : "memory");
  __asm__ volatile ("li x30, 0" : : : "memory");
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
verify_trigger (int correct)
{
  __asm__ volatile ("li x29, 0x101010" : : : "memory");
  __asm__ volatile ("li x30, 2" : : : "memory");
  __asm__ volatile ("fence.i" : : : "memory");
  __asm__ volatile ("li x29, 0" : : : "memory");
  __asm__ volatile ("li x30, 0" : : : "memory");
}
