
.section ".init"

.globl entry
entry:  
  csrrs t0, mhartid, x0 #read current hart id
  bne t0, x0, start_hart1#branch to infloop if hart id is not zero, if hsc is enabled, we start with only one hart
  # bne t0, x0, infloop

pre_hart0:
    la a0, pre_hart1
    li a1, 0x0

    csrrw x0, hscworkid, x0
    csrrw x0, hscworkpc, a0
    csrrw x0, hscworkcap, a1
    hscswitch a0
    j start_hart0

pre_hart1:
    li a0, 1 #hsc id
    la a1, start_hart1
    li a2, 0x0

    csrrw x0, hscworkid, a0
    csrrw x0, hscworkpc, a1
    csrrw x0, hscworkcap, a2
    hscstart a0


start_hart0:
  li    t0, 1
  sw    t0, 0(x0) # address 0 contains the number of harts
  la    sp, __stack_top
  j     _start 

start_hart1:
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  nop
  li    t0, 2
  sw    t0, 0(x0) # address 0 contains the number of harts

  la    sp, __stack_top2
  j     _start

.globl get_hart_id
get_hart_id:
  csrrs a0, mhartid, x0
  ret

.globl get_hart_count
get_hart_count:
  lw a0, 0(x0) # 
  ret

.globl hsc_switch_in_place
hsc_switch_in_place:
  csrrw x0, hscworkid, a0
  csrrw x0, hscworkcap, a1
  csrrw x0, hscworkpc, ra

  hscswitch a0
  ret #in case of no HSC support

.globl infloop
infloop:
  nop
  nop
  nop
  nop
  j infloop

.globl wait_for_sync_loop
wait_for_sync_loop:
    nop
    nop
    fence.i
    nop
    nop
    j wait_for_sync_loop

#write "ret" instruction (0x00008067) at address [a0]
#prevent the compiler to write several sh instruction that can mess up syncXXX functions
.globl pseudo_atomic_ret_write
pseudo_atomic_ret_write:
    lui     t0,0xffff8
    addi  	t0,t0,103 # ffff8067 
    li      t1,0xffff
    and     t0, t0, t1
    sw      t0, 0(a0)
    fence.i
    ret

.globl write_u32
write_u32:
  sw  a0, 0(a1)
  ret